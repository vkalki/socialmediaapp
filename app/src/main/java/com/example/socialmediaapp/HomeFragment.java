package com.example.socialmediaapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


/**
 * The home page 'fragment'. Users can interact with informational guides related
 * to taking proper care of puppies and other animals based on the displayed feed.
 */
public class HomeFragment extends Fragment {

    //firebase auth
    private FirebaseAuth firebaseAuth;


    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Initialize on startup
     *
     * @param inflater           create the homepage
     * @param container          create the objects on the homepage
     * @param savedInstanceState store the previous page the user was on
     * @return the created page
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        //init
        firebaseAuth = FirebaseAuth.getInstance();

        return view;
    }

    /**
     * Determines if the user is already signed in (show the dashboard)
     * or if the user is yet to sign in (hide the dashboard)
     */
    private void checkUserStatus() {
        //get current user
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            //if signed in, stay at profile
            //set email of logged in user
            //mProfileTv.setText(user.getEmail());

        } else {
            //if not signed in, go to main (login)
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        }
    }

    /**
     * Initialize on startup
     *
     * @param savedInstanceState store the prior page the user was on
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true); //show menu option in fragment
        super.onCreate(savedInstanceState);
    }

    /**
     * Inflate options menu
     *
     * @param menu the selections given to the user
     * @return the created menu
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflate menu
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Handle menu item clicks
     *
     * @param item the item in the menu to be interacted with
     * @return the selected item
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //get item id
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            firebaseAuth.signOut();
            checkUserStatus();
        }
        return super.onOptionsItemSelected(item);
    }
}
