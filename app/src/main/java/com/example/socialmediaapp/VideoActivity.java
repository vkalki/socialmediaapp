package com.example.socialmediaapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.appcompat.app.AppCompatActivity;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.util.ArrayList;

/**
 * Service to enable and connect a Live RTSP stream from an IP camera with the
 * application. Since the stream is live, it needs to be decoded using the Open Source
 * VLC package, the feed supported currently includes h264 which supports video and audio playback.
 */
public class VideoActivity extends AppCompatActivity implements IVLCVout.Callback {

    //test case
    @SuppressLint("AuthLeak")
    public static final String RTSP_URL = "rtsp://Admin:Password@192.168.1.22/live";

    private SurfaceHolder holder;

    // media player
    private LibVLC libvlc;
    private MediaPlayer mMediaPlayer = null;

    /**
     * Initialize on startup
     *
     * @param savedInstanceState store the prior page the user was on
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        // Get URL
        Intent intent = getIntent();
        String rtspUrl = intent.getExtras().getString(RTSP_URL);

        // display surface
        SurfaceView mSurface = findViewById(R.id.surface);
        holder = mSurface.getHolder();

        ArrayList<String> options = new ArrayList<String>();
        options.add("--aout=opensles");
        options.add("--audio-time-stretch"); // time stretching
        options.add("-vvv"); // verbosity
        options.add("--aout=opensles");
        options.add("--avcodec-codec=h264");


        libvlc = new LibVLC(getApplicationContext(), options);
        holder.setKeepScreenOn(true);

        // Create media player
        mMediaPlayer = new MediaPlayer(libvlc);

        // Set up video output
        final IVLCVout vout = mMediaPlayer.getVLCVout();
        vout.setVideoView(mSurface);
        vout.addCallback(this);
        vout.attachViews();

        Media m = new Media(libvlc, Uri.parse(rtspUrl));

        mMediaPlayer.setMedia(m);
        mMediaPlayer.play();
    }

    /**
     * On application resume, reconnect the player
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * On application pause, disconnect the player
     */
    @Override
    protected void onPause() {
        super.onPause();
        releasePlayer();
    }

    /**
     * On application closure, shut down the player
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    // Required unused method
    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {

    }

    // Required unused method
    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {

    }

    // Required unused method
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    /**
     * On application closure or back button pressed, close the player
     */
    public void releasePlayer() {
        if (libvlc == null)
            return;
        mMediaPlayer.stop();
        final IVLCVout vout = mMediaPlayer.getVLCVout();
        vout.removeCallback(this);
        vout.detachViews();
        holder = null;
        libvlc.release();
        libvlc = null;
    }
}