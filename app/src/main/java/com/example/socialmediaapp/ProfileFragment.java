package com.example.socialmediaapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.HashSet;

import static android.app.Activity.RESULT_OK;
import static com.google.firebase.storage.FirebaseStorage.getInstance;


/**
 * The profile page fragment handling the users information
 * as well as updating that information. Communicated with
 * firebase to store images, text, and status.
 */
public class ProfileFragment extends Fragment {

    //firebase
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    //storage
    private StorageReference storageReference;

    //views from xml
    private ImageView avatarIv, coverIv;
    private TextView nameTv, emailTv, phoneTv;

    //progress dialog
    private ProgressDialog progressDialog;

    //permissions constants
    private static final int CAMERA_REQUEST_CODE = 100;
    private static final int STORAGE_REQUEST_CODE = 200;
    private static final int IMAGE_PICK_GALLERY_CODE = 300;
    private static final int IMAGE_PICK_CAMERA_CODE = 400;

    //arrays of permissions
    private String[] cameraPerm;
    private String[] storagePerm;

    private String imageType;    //checking for profile or cover photo

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Initialize on startup
     *
     * @param inflater           create the profile page
     * @param container          create the objects on the profile page
     * @param savedInstanceState store the previous page the user was on
     * @return the created page
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        //init firebase
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Users");
        storageReference = getInstance().getReference().child("Images"); //firebase storage reference

        //init arrays of permissions
        cameraPerm = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        storagePerm = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        //init views
        avatarIv = view.findViewById(R.id.avatarIv);
        coverIv = view.findViewById(R.id.coverIv);
        nameTv = view.findViewById(R.id.nameTv);
        emailTv = view.findViewById(R.id.emailTv);
        phoneTv = view.findViewById(R.id.phoneTv);

        //action button
        FloatingActionButton fab = view.findViewById(R.id.fab);

        //init pd
        progressDialog = new ProgressDialog(getActivity());

        /*Get the info of the currently signed in user. Use the UID or Email. Utilize byOrderChild query to get user nodes*/
        Query query = databaseReference.orderByChild("email").equalTo(firebaseUser.getEmail());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //check until required data is received
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    //get data
                    String name = "" + ds.child("name").getValue();
                    String email = "" + ds.child("email").getValue();
                    String phone = "" + ds.child("phone").getValue();
                    String image = "" + ds.child("image").getValue();
                    String cover = "" + ds.child("cover").getValue();

                    //set data
                    nameTv.setText(name);
                    emailTv.setText(email);
                    phoneTv.setText(phone);

                    try {
                        //if received
                        Picasso.get().load(image).centerCrop().fit().into(avatarIv);
                    } catch (Exception e) {
                        //handle issue
                    }

                    try {
                        //if received
                        Picasso.get().load(cover).fit().into(coverIv);
                    } catch (Exception e) {
                        //handle issue
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //fab button click
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditProfileDialog();
            }
        });

        return view;
    }

    /**
     * check if the app has permissions to store
     *
     * @return boolean of permission state
     */
    private boolean checkStoragePermission() {
        //check storage permission

        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == (PackageManager.PERMISSION_GRANTED);
    }

    /**
     * if the app does not have permissions to store, prompt a request
     */
    private void requestStoragePermission() {
        //runtime request storage perms
        requestPermissions(storagePerm, STORAGE_REQUEST_CODE);
    }

    /**
     * check if the app has permissions to access camera
     *
     * @return boolean of permission state
     */
    private boolean checkCameraPermission() {
        //check storage permission
        boolean result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == (PackageManager.PERMISSION_GRANTED);

        boolean result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == (PackageManager.PERMISSION_GRANTED);

        return result && result1;
    }

    /**
     * If the app does not have permissions to access camera, prompt a request
     */
    private void requestCameraPermission() {
        //runtime request storage perms
        requestPermissions(cameraPerm, CAMERA_REQUEST_CODE);
    }

    /**
     * Enable a visual menu for the user to interact with to edit their profile
     */
    private void showEditProfileDialog() {
        // options
        String[] options = {"Edit Profile Picture", "Edit Cover Photo", "Edit Name", "Edit Phone"};
        // alert
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // set title
        builder.setTitle("Choose Action");
        //set items to dialog
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //handle item clicks
                if (which == 0) {
                    //edit profile on click
                    progressDialog.setMessage("Updating Profile Picture");
                    imageType = "image"; //changing the profile picture
                    showImagePicDialog();

                } else if (which == 1) {
                    //edit cover on click
                    progressDialog.setMessage("Updating Cover Photo");
                    imageType = "cover"; //changing the profile picture
                    showImagePicDialog();

                } else if (which == 2) {
                    //edit name on click
                    progressDialog.setMessage("Updating Name");
                    showNamePhoneUpdateDialog("name"); //changing the name

                } else if (which == 3) {
                    //edit number on click
                    progressDialog.setMessage("Updating Phone");
                    showNamePhoneUpdateDialog("phone"); //changing the number
                }
            }
        });
        //create and show dialog
        builder.create().show();
    }

    /**
     * Prompt the user to change the options displayed on their profile when selected
     *
     * @param inKey the change requested
     */
    private void showNamePhoneUpdateDialog(String inKey) {
        //key contains either the name or the phone
        final String key = inKey;

        //custom dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Update " + key); // update name or update number

        //set layout of the dialog
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        //add edit text
        final EditText editText = new EditText(getActivity());
        editText.setHint("Enter " + key);
        linearLayout.addView(editText);

        builder.setView(linearLayout);

        //add button in dialog to update
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //input text from edit text
                String value = editText.getText().toString().trim();
                //validate if user has entered something or not
                if (!TextUtils.isEmpty(value)) {
                    progressDialog.show();
                    HashMap<String, Object> result = new HashMap<>();
                    result.put(key, value);

                    databaseReference.child(firebaseUser.getUid()).updateChildren(result)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    //updated dismiss dialog
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Updated...", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    //failed, dismiss progress and show error
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                } else {
                    Toast.makeText(getActivity(), "Please enter " + key, Toast.LENGTH_SHORT).show();
                }
            }
        });

        //add button in dialog to cancel
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();

    }

    /**
     * Prompt the user with the gallery or camera if they want to upload a new profile
     * or banner picture for their profile.
     */
    private void showImagePicDialog() {
        //show options between camera and gallery
        // options
        String[] options = {"Camera", "Gallery"};
        // alert
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // set title
        builder.setTitle("Select an Image From");
        //set items to dialog
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //handle item clicks
                if (which == 0) {
                    // Camera Clicked
                    if (!checkCameraPermission()) {
                        requestCameraPermission();
                    } else {
                        pickFromCamera();
                    }

                } else if (which == 1) {
                    // Gallery Clicked
                    if (!checkStoragePermission()) {
                        requestStoragePermission();
                    } else {
                        pickFromGallery();
                    }

                }
            }
        });
        builder.create().show();
    }

    /**
     * Request camera access
     *
     * @param requestCode  camera access code
     * @param permissions  determine android permissions
     * @param grantResults determine if permission was granted
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //allow or deny permissions dialog --> handle cases allow and deny

        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                //picking from camera --> check perms
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cameraAccepted && writeStorageAccepted) {
                        //perm given
                        pickFromCamera();
                    } else {
                        //perm denied
                        Toast.makeText(getActivity(), "Please enable camera & storage permission", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            case STORAGE_REQUEST_CODE: {
                //picking from storage --> check perms
                if (grantResults.length > 0) {
                    boolean writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (writeStorageAccepted) {
                        //perm given
                        pickFromGallery();
                    } else {
                        //perm denied
                        Toast.makeText(getActivity(), "Please enable storage permission", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Handle information being returned by the user after prompting for it
     *
     * @param requestCode code requested by the system
     * @param resultCode  code returned by the user
     * @param data        information returned by the user
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && data != null && data.getData() != null) {
            if (requestCode == IMAGE_PICK_CAMERA_CODE || requestCode == IMAGE_PICK_GALLERY_CODE) {
                Uri file = data.getData();
                uploadProfileCoverPhoto(file);
            }
        }
    }

    /**
     * Upload an image from the gallery or camera into the Firebase Database
     *
     * @param uri the image to be uploaded
     */
    private void uploadProfileCoverPhoto(final Uri uri) {
        //show progress
        progressDialog.show();

        final StorageReference imageStore = storageReference.child("image" + uri.getLastPathSegment());

        imageStore.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                imageStore.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        DatabaseReference imageBase = FirebaseDatabase.getInstance().getReference().child("Users");

                        HashMap<String, Object> result = new HashMap<>();
                        result.put(imageType, String.valueOf(uri));

                        imageBase.child(firebaseUser.getUid()).updateChildren(result).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getActivity(), "Success!", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "An issue occurred with the Database!", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();

                            }
                        });

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "An issue occurred with the Storage", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }
                });


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), "An issue occurred...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Select from the camera using the Android Camera Activity
     */
    private void pickFromCamera() {
        //intent of picking an image from the camera
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "Temp Pic");
        values.put(MediaStore.Images.Media.DESCRIPTION, "Temp Description");
        //put image uri
        //uri of picked image
        Uri filePath = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        //start camera
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, filePath);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);

    }

    /**
     * Select from the gallery using the Android Gallery Activity
     */
    private void pickFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, IMAGE_PICK_GALLERY_CODE);
    }

    /**
     * Determines if the user is already signed in (show the dashboard)
     * or if the user is yet to sign in (hide the dashboard)
     */
    private void checkUserStatus() {
        //get current user
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            //to be implemented
        } else {
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        }
    }

    /**
     * Initialize on startup
     *
     * @param savedInstanceState store the prior page the user was on
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true); //show menu option in fragment
        super.onCreate(savedInstanceState);
    }

    /**
     * Inflate options menu
     *
     * @param menu the selections given to the user
     * @return the created menu
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflate menu
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Handle menu item clicks
     *
     * @param item the item in the menu to be interacted with
     * @return the selected item
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //get item id
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            firebaseAuth.signOut();
            checkUserStatus();
        }
        return super.onOptionsItemSelected(item);
    }
}
