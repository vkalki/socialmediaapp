package com.example.socialmediaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Creates a new Dashboard object to allow quick 'fragment' swapping. This dashboard activity
 * remains persistent throughout each of the fragment pages and only appears after the user has
 * logged in. Fragments do not store data locally each time in an attempt to prevent user
 * side manipulation of data.
 */
public class DashboardActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth; //firebase auth
    private ActionBar actionBar; //action bar

    /**
     * Initialize on startup
     *
     * @param savedInstanceState store the prior page the user was on
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        actionBar = getSupportActionBar();
        actionBar.setTitle("Profile");

        //init
        firebaseAuth = FirebaseAuth.getInstance();

        //bottom navigation
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(selectedListener);

        //default fragment (fragment, start)
        actionBar.setTitle("Home");
        HomeFragment homeFragment = new HomeFragment();
        FragmentTransaction homeTransaction = getSupportFragmentManager().beginTransaction();
        homeTransaction.replace(R.id.content, homeFragment, "");
        homeTransaction.commit();
    }

    /**
     * Handles the selection of each option in the navigational pane
     */
    private BottomNavigationView.OnNavigationItemSelectedListener selectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            //handle item clicks
            switch (item.getItemId()) {

                case R.id.nav_home:
                    //home fragment
                    actionBar.setTitle("Home");
                    HomeFragment homeFragment = new HomeFragment();
                    FragmentTransaction homeTransaction = getSupportFragmentManager().beginTransaction();
                    homeTransaction.replace(R.id.content, homeFragment, "");
                    homeTransaction.commit();
                    return true;

                case R.id.nav_profile:
                    //profile fragment
                    actionBar.setTitle("Profile");
                    ProfileFragment profileFragment = new ProfileFragment();
                    FragmentTransaction profileTransaction = getSupportFragmentManager().beginTransaction();
                    profileTransaction.replace(R.id.content, profileFragment, "");
                    profileTransaction.commit();
                    return true;

                case R.id.nav_users:
                    //users fragment
                    actionBar.setTitle("Users");
                    UsersFragment userFragment = new UsersFragment();
                    FragmentTransaction usersTransaction = getSupportFragmentManager().beginTransaction();
                    usersTransaction.replace(R.id.content, userFragment, "");
                    usersTransaction.commit();
                    return true;

                case R.id.nav_video:
                    //users fragment
                    actionBar.setTitle("Video Feed");

                    //startActivity(new Intent(DashboardActivity.this, VideoActivity.class));

                    VideoFragment chatListFragment = new VideoFragment();
                    FragmentTransaction chatListTransaction = getSupportFragmentManager().beginTransaction();
                    chatListTransaction.replace(R.id.content, chatListFragment, "");
                    chatListTransaction.commit();
                    return true;

            }
            return false;
        }
    };

    /**
     * Determines if the user is already signed in (show the dashboard)
     * or if the user is yet to sign in (hide the dashboard)
     */
    private void checkUserStatus() {
        //get current user
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            //to be implemented, handle logged in user
        } else {
            //if not signed in, go to main (login)
            startActivity(new Intent(DashboardActivity.this, MainActivity.class));
            finish();
        }
    }

    /**
     * User presses the back button
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * On app startup
     */
    @Override
    protected void onStart() {
        //check on start of app
        checkUserStatus();
        super.onStart();
    }
}
