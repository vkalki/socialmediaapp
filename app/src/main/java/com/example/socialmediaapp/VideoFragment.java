package com.example.socialmediaapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * A video fragment page used to handle input and request for video feed from users.
 * Allows users to input a valid RTSP url to stream using VLC media player which launches
 * after selection.
 */
public class VideoFragment extends Fragment {

    public VideoFragment() {
        // Required empty public constructor
    }

    /**
     * Initialize on startup
     *
     * @param inflater           create the video page
     * @param container          create the objects on the video page
     * @param savedInstanceState store the previous page the user was on
     * @return the created page
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_video, container, false);

        Button startButton = view.findViewById(R.id.buttonStart);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), VideoActivity.class);
                EditText textRTSP = view.findViewById(R.id.RTSPurlEt);
                intent.putExtra(VideoActivity.RTSP_URL, textRTSP.getText().toString());
                startActivity(intent);
            }
        });

        return view;
    }

}
