package com.example.socialmediaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.socialmediaapp.adapters.AdapterChat;
import com.example.socialmediaapp.models.ModelChat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Class defines the ability for users to send messages to one another which are stored
 * on the Firebase realtime database. Messages contain information that reflects the
 * time which the message is send, the sender and receiver of the message, the contents
 * of the message, and the profile of the user who is sending the message. Messages sent
 * can be read and deleted based on the users preferences, strongly influenced by IG.
 */
public class ChatActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ImageView otherProfileIv;
    private TextView nameTv, userStatusTv;
    private EditText messageEt;

    private FirebaseAuth firebaseAuth;

    private List<ModelChat> chatList;
    private AdapterChat adapterChat;

    private String otherUID;
    private String myUID;
    private String otherImage;

    /**
     * On startup, present the user with information tied to their unique Firebase ID
     *
     * @param savedInstanceState if the user minimizes the app and opens it again, store its prior state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //init views
        //views from xml
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        recyclerView = findViewById(R.id.chat_recyclerView);
        otherProfileIv = findViewById(R.id.otherProfileIv);
        nameTv = findViewById(R.id.otherNameTv);
        userStatusTv = findViewById(R.id.userStatusTv);
        messageEt = findViewById(R.id.messageEt);
        ImageButton sendBtn = findViewById(R.id.sendBtn);

        //layout for recyclerview
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);

        //recyclerview properties
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        //onclick, grab uid to chat
        otherUID = getIntent().getStringExtra("otherUID");

        //init firebase
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        DatabaseReference userDbRef = firebaseDatabase.getReference("Users");

        //search user to get that user's information
        Query userQuery = userDbRef.orderByChild("uid").equalTo(otherUID);

        //get user picture
        userQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //check until required info is received
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    //get data
                    String name = "" + snapshot.child("name").getValue();
                    otherImage = "" + snapshot.child("image").getValue();
                    String typingStatus = "" + snapshot.child("typingTo").getValue();

                    //check typing status
                    if (typingStatus.equals(myUID)) {
                        userStatusTv.setText("Typing...");
                    } else {
                        //get onlinestatus
                        String onlineStatus = "" + snapshot.child("onlineStatus").getValue();
                        if (onlineStatus.equals("online")) {
                            userStatusTv.setText(onlineStatus);
                        } else {
                            //convert timestamp to proper time and date
                            //convert timestamp to dd/mm/yyyy

                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(Long.parseLong(onlineStatus));
                            String dateTime = DateFormat.format("MM/dd/yy", cal).toString();
                            userStatusTv.setText("Last seen: " + dateTime);

                        }
                    }

                    //set data
                    nameTv.setText(name);

                    try {
                        //if received
                        Picasso.get().load(otherImage).fit().into(otherProfileIv);
                    } catch (Exception e) {
                        //handle issue
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        //click button to send a message
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get text from edit text
                String message = messageEt.getText().toString().trim();

                //check if text is empty
                if (TextUtils.isEmpty(message)) {
                    //text empty
                    Toast.makeText(ChatActivity.this, "Send a message!", Toast.LENGTH_SHORT).show();
                } else {
                    //text not empty
                    sendMessage(message);
                    recyclerView.scrollToPosition(chatList.size());
                }
            }
        });

        //check edit text change listener
        messageEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() == 0) {
                    checkTypingStatus("none");
                } else {
                    checkTypingStatus(otherUID);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        readMessages();
    }

    /**
     * Display messages to the active user. All messages are stored in the Firebase server before displaying.
     */
    private void readMessages() {
        chatList = new ArrayList<>();
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("Chats");

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                chatList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ModelChat chat = snapshot.getValue(ModelChat.class);

                    if (chat.getReceiver().equals(myUID) && chat.getSender().equals(otherUID) ||
                            chat.getReceiver().equals(otherUID) && chat.getSender().equals(myUID)) {
                        chatList.add(chat);
                    }

                    //adapter
                    adapterChat = new AdapterChat(ChatActivity.this, chatList, otherImage);
                    adapterChat.notifyDataSetChanged();

                    //set adapter chat to recycle view
                    recyclerView.setAdapter(adapterChat);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /**
     * Send a new message to a designated target
     *
     * @param message the message to be sent
     */
    private void sendMessage(String message) {

        //create a message node whenever a user sends a message to another user

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        String timestamp = String.valueOf(System.currentTimeMillis());

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender", myUID);
        hashMap.put("receiver", otherUID);
        hashMap.put("message", message);
        hashMap.put("timestamp", timestamp);

        databaseReference.child("Chats").push().setValue(hashMap);

        //reset edit text after sending a message
        messageEt.setText("");

    }

    /**
     * Determines if the user is already signed in (show the dashboard)
     * or if the user is yet to sign in (hide the dashboard)
     */
    private void checkUserStatus() {
        //get current user
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            myUID = user.getUid();
        } else {
            //if not signed in, go to main (login)
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    /**
     * Determines if the user is already signed in (show they are online)
     * or if the user is offline (show they are offline to others)
     *
     * @param status the current state of the user
     */
    private void checkOnlineStatus(String status) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(myUID);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("onlineStatus", status);

        //update value of onlineStatus for the current user
        databaseReference.updateChildren(hashMap);
    }

    /**
     * Display if the user is actively typing to another user in the information bar
     *
     * @param typing
     */
    private void checkTypingStatus(String typing) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(myUID);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("typingTo", typing);

        //update value of onlineStatus for the current user
        databaseReference.updateChildren(hashMap);

    }

    /**
     * On initial app start
     */
    @Override
    protected void onStart() {
        checkUserStatus();
        checkOnlineStatus("online");
        super.onStart();
    }

    /**
     * When app is minimized
     */
    @Override
    protected void onPause() {
        super.onPause();

        //get timestamp
        String timestamp = String.valueOf(System.currentTimeMillis());

        //set offline with last seen timestamp
        checkOnlineStatus(timestamp);
        checkTypingStatus("none");
    }

    /**
     * When app is continued
     */
    @Override
    protected void onResume() {
        checkOnlineStatus("online");
        super.onResume();
    }

    /**
     * Enable users to access the logout menu from this page
     *
     * @param item the menu
     * @return the desired result the user wants to do (logout / cancel)
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        //get item id
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            firebaseAuth.signOut();
            checkUserStatus();
        }

        return super.onOptionsItemSelected(item);
    }
}
