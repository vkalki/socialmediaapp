package com.example.socialmediaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.socialmediaapp.ChatActivity;
import com.example.socialmediaapp.R;
import com.example.socialmediaapp.models.ModelUser;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Class defines the assimilation of information from a users stored values inside of Firebase
 * and displays it on the universal user list. The list is updated and can be restricted to only
 * certain parameters depending on the desired target of the active user.
 */
public class AdapterUsers extends RecyclerView.Adapter<AdapterUsers.MyHolder> {

    private Context context;
    private List<ModelUser> userList;

    /**
     * Constructor used to define the user in question
     *
     * @param context  the current page (with search parameters)
     * @param userList the users registered with the service
     */
    public AdapterUsers(Context context, List<ModelUser> userList) {
        this.context = context;
        this.userList = userList;
    }

    /**
     * Display the fragment
     *
     * @param parent   previous page
     * @param viewType type of fragment: normal view
     * @return the view displayed to the user
     */
    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_users, parent, false);

        return new MyHolder(view);
    }

    /**
     * Update information on the users screen from the Firebase Database
     */
    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        //get data
        final String otherUID = userList.get(position).getUid();
        String userImage = userList.get(position).getImage();
        String userName = userList.get(position).getName();
        final String userEmail = userList.get(position).getEmail();

        //set data
        holder.mNameTv.setText(userName);
        holder.mEmailTv.setText(userEmail);

        try {
            Picasso.get().load(userImage).placeholder(R.drawable.ic_default_img).into(holder.mAvatarIv);
        } catch (Exception ignored) {

        }

        //handle item click
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start message activity of the clicked UID
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("otherUID", otherUID);
                context.startActivity(intent);
            }
        });


    }

    /**
     * @return number of chats sent
     */
    @Override
    public int getItemCount() {
        return userList.size();
    }

    //internal view holder class (bean)
    class MyHolder extends RecyclerView.ViewHolder {

        ImageView mAvatarIv;
        TextView mNameTv, mEmailTv;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            mAvatarIv = itemView.findViewById(R.id.avatarIv);
            mNameTv = itemView.findViewById(R.id.nameTv);
            mEmailTv = itemView.findViewById(R.id.emailTv);

        }
    }


}
